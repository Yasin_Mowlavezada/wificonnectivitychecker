package com.pomtech.yasin.wirelessconectionsfirst;

        import android.content.Context;
        import android.content.IntentFilter;
        import android.net.wifi.WifiManager;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    static String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        IntentFilter intentFilter = new IntentFilter();
        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        name = wifiManager.getConnectionInfo().getSSID();
        intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        registerReceiver( new MyBroadCast(),intentFilter);
    }
}
