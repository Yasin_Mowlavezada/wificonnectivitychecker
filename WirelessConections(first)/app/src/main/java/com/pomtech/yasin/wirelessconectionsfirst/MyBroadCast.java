package com.pomtech.yasin.wirelessconectionsfirst;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.widget.Toast;

/**
 * Created by M.Yasin on 4/11/2017.
 */

public class MyBroadCast extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        final  String action = intent.getAction();

        if(action.equalsIgnoreCase(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION)){
            if(intent.getBooleanExtra(WifiManager.EXTRA_SUPPLICANT_CONNECTED , false)){
                Toast.makeText(context , "Connected to " + MainActivity.name , Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(context , "Disconnected" , Toast.LENGTH_LONG).show();
            }
        }

    }
}
